import 'dart:io';

abstract class Employee {
  void fullName();
  void employeeDept();
  void employeeSalary() {
    print('your salasy is 4000');
  }
}

abstract class Job {
  void myfunction();
}

class Student {
  int age = 0;
  String name = '';

  void displayName() {
    print('My name is $name');
  }

  void displayAge() {
    print('My age is $age');
  }
}

class Faculty {
  int salay = 0;
  String depname = '';

  void displayDepartment() {
    print('I am a Prof of $depname');
  }

  void displaySalay() {
    print('My salary is $salay');
  }
}

class College implements Student, Faculty {
  @override
  int age = 0;

  @override
  String depname = '';

  @override
  String name = '';

  @override
  int salay = 0;

  @override
  void displayAge() {
    print('My age is $age');
  }

  @override
  void displayDepartment() {
    print('I am a Prof of $depname');
  }

  @override
  void displayName() {
    print('My name is $name');
  }

  @override
  void displaySalay() {
    print('My salary is $salay');
  }
}

class Muhammed extends Employee implements Job {
  @override
  employeeDept() {
    print('your department is IT');
  }

  @override
  fullName() {
    print('your full name is Muhammed Essa Hameed');
  }

  @override
  void myfunction() {
    print('this is my function from job class');
  }
}

void main() {
  var muhammed = new Muhammed();
  College cg = College();
  cg.name = 'Takitclhok';
  cg.age = 21;
  cg.depname = 'CS';
  cg.salay = 18000;

  cg.displayName();
  cg.displayAge();
  cg.displayDepartment();
  cg.displaySalay();

  // muhammed.employeeDept();
  // muhammed.employeeSalary();
  // muhammed.fullName();
  // muhammed.myfunction();
}
